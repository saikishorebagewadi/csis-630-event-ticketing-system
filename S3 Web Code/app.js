const GETALLEVENTS_URL='https://p6khhtdb8l.execute-api.us-east-1.amazonaws.com/alpha/getevents';
const GETPRICESOFEVENT_URL='https://p6khhtdb8l.execute-api.us-east-1.amazonaws.com/alpha/getticketpricesforevent';
const POSTPURCHASE_URL= 'https://p6khhtdb8l.execute-api.us-east-1.amazonaws.com/alpha/purchasetickets';
const EventListElement = document.querySelector('.eventList');
const ticketpricesElement = document.querySelector('.ticketprices'); //same as above element but a diff handlername
const GetAllEventsElement = document.querySelector('.GetAllEvents');

async function GetAllEvents()
{
    const response = await fetch(GETALLEVENTS_URL)
    const json = await response.json()
    console.log(json);
//     <!--div class="column">
//     <div class="card">
//         <div class="card-image">
//           <figure class="image is-4by3">
//             <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
//           </figure>
//         </div>
//         <div class="card-content">
//           <div class="media">
//             <div class="media-content">
//               <p class="title is-4">John Smith</p>
//               <p class="subtitle is-6">@johnsmith</p>
//             </div>
//           </div>
//         </div>
//       </div>
// </!--div>
EventListElement.innerHTML='';
json.forEach(event => {
    
    const columnElement = document.createElement('div');
    columnElement.classList.add('column');
    const cardElement = document.createElement('div');
    cardElement.classList.add('card');
    const cardImageElement = document.createElement('div');
    cardImageElement.classList.add('card-image');
    const imageis4by3Element = document.createElement('figure');
    imageis4by3Element.classList.add('image');
    imageis4by3Element.classList.add('is-4by3');
    const eventImageElement = document.createElement('img');
    eventImageElement.setAttribute('src','https://bulma.io/images/placeholders/1280x960.png');
    imageis4by3Element.appendChild(eventImageElement);
    cardImageElement.appendChild(imageis4by3Element);
    cardElement.appendChild(cardImageElement);

    const CardContentElement  = document.createElement('div');
    CardContentElement.classList.add('card-content');
    const mediaElement = document.createElement('div');
    mediaElement.classList.add('media');
    const mediacontentElement = document.createElement('div');
    mediacontentElement.classList.add('media-content');
    const titleis4Element = document.createElement('p');
    titleis4Element.innerText = event['EventID'];
    titleis4Element.classList.add('title');
    titleis4Element.classList.add('is-4');
    const subtitleis6Element = document.createElement('p');
    subtitleis6Element.innerText = event['EventName'];
    subtitleis6Element.classList.add('subtitle');
    subtitleis6Element.classList.add('is-6');
//     <div class="button-section">
//<button class="button is-primary is-rounded">Rounded</button>
//     <a class="button is-info GetAllEvents">
//       Search All Events
//   </a>
//   </div>
    const buttonsectionElement  = document.createElement('div');
    buttonsectionElement.classList.add('button-section');
        const buttonisprimaryroundedElement = document.createElement('button');
        buttonisprimaryroundedElement.classList.add('button');
        buttonisprimaryroundedElement.classList.add('is-primary');
        buttonisprimaryroundedElement.classList.add('is-rounded');
        buttonisprimaryroundedElement.classList.add('EventID'+event['EventID']);
        buttonisprimaryroundedElement.classList.add('lookuptickets');
        buttonisprimaryroundedElement.innerText = 'Look Tickets Up';
        buttonisprimaryroundedElement.addEventListener('click', LookupTickets);
        buttonisprimaryroundedElement.EventID = event['EventID'];
    buttonsectionElement.appendChild(buttonisprimaryroundedElement);

    mediacontentElement.appendChild(titleis4Element);
    mediacontentElement.appendChild(subtitleis6Element);
    mediacontentElement.appendChild(buttonsectionElement);
    mediaElement.appendChild(mediacontentElement);
    CardContentElement.appendChild(mediaElement);
    cardElement.appendChild(CardContentElement);

    columnElement.appendChild(cardElement);
    EventListElement.appendChild(columnElement);
});



}

async function LookupTickets(Event)
{
    ticketpricesElement.innerHTML='';
    const response = await fetch(GETPRICESOFEVENT_URL+ "?eventid=" + Event.currentTarget.EventID);
    const json = await response.json();
    console.log(json);
    // alert(Event.currentTarget.EventID);

//     <div class="column">
//     <div class="card">
//         <div class="card-content">
//           <div class="media">
//             <div class="media-content">
//               <p class="title is-4">John Smith</p>
//               <p class="subtitle is-6">@johnsmith</p>
//             </div>
//           </div>
//         </div>
//       </div>
// </div>

json.forEach(event => {
    
    const columnElement = document.createElement('div');
    columnElement.classList.add('column');
    const cardElement = document.createElement('div');
    cardElement.classList.add('card');
   
    const CardContentElement  = document.createElement('div');
    CardContentElement.classList.add('card-content');
    const mediaElement = document.createElement('div');
    mediaElement.classList.add('media');
    const mediacontentElement = document.createElement('div');
    mediacontentElement.classList.add('media-content');
    const titleis4Element = document.createElement('p');
    titleis4Element.innerText = 'EventID: ' + event['EventID'];
    titleis4Element.classList.add('title');
    titleis4Element.classList.add('is-4');
    const subtitleis6TicketPriceElement = document.createElement('p');
    subtitleis6TicketPriceElement.innerText = 'Price of the ticket:'+ event['TicketPrice'];
    subtitleis6TicketPriceElement.classList.add('subtitle');
    subtitleis6TicketPriceElement.classList.add('is-6');

    const subtitleis6TotalTicketsElement = document.createElement('p');
    subtitleis6TotalTicketsElement.innerText = 'Total tickets:'+event['TotalTickets'];
    subtitleis6TotalTicketsElement.classList.add('subtitle');
    subtitleis6TotalTicketsElement.classList.add('is-6');

    const subtitleis6TicketsSoldElement = document.createElement('p');
    subtitleis6TicketsSoldElement.innerText = 'Tickets sold: '+ event['TicketsSold'];
    subtitleis6TicketsSoldElement.classList.add('subtitle');
    subtitleis6TicketsSoldElement.classList.add('is-6');

    const subtitleis6TicketsAvailableElement = document.createElement('p');
    subtitleis6TicketsAvailableElement.innerText = 'Tickets Available: ' +  event['TicketsAvailable'];
    subtitleis6TicketsAvailableElement.classList.add('subtitle');
    subtitleis6TicketsAvailableElement.classList.add('is-6');
//     <div class="button-section">
//<button class="button is-primary is-rounded">Rounded</button>
//     <a class="button is-info GetAllEvents">
//       Search All Events
//   </a>
//   </div>
    const buttonsectionElement  = document.createElement('div');
    buttonsectionElement.classList.add('button-section');
        const buttonisprimaryroundedElement = document.createElement('button');
        buttonisprimaryroundedElement.classList.add('button');
        buttonisprimaryroundedElement.classList.add('is-primary');
        buttonisprimaryroundedElement.classList.add('is-rounded');
        buttonisprimaryroundedElement.classList.add('EventID'+event['EventID']);
        buttonisprimaryroundedElement.classList.add('lookuptickets');
        buttonisprimaryroundedElement.innerText = 'Purchase Tickets';
        buttonisprimaryroundedElement.addEventListener('click', OpenPurchaseModal);
        buttonisprimaryroundedElement.TicketPrice = event['TicketPrice'];
        buttonisprimaryroundedElement.EventID = event['EventID'];

    buttonsectionElement.appendChild(buttonisprimaryroundedElement);

    mediacontentElement.appendChild(titleis4Element);
    mediacontentElement.appendChild(subtitleis6TicketPriceElement);
    mediacontentElement.appendChild(subtitleis6TicketsAvailableElement);
    mediacontentElement.appendChild(subtitleis6TicketsSoldElement);
    mediacontentElement.appendChild(subtitleis6TotalTicketsElement);
    mediacontentElement.appendChild(buttonsectionElement);

    
     

    mediaElement.appendChild(mediacontentElement);
    CardContentElement.appendChild(mediaElement);
    cardElement.appendChild(CardContentElement);

    columnElement.appendChild(cardElement);
    ticketpricesElement.appendChild(columnElement);

    });


}
async function OpenPurchaseModal(context)
{
//   alert(context.currentTarget.TicketPrice);
  ticketpricesElement.innerHTML='';
  const columnElement = document.createElement('div');
    columnElement.classList.add('column');
    const cardElement = document.createElement('div');
    cardElement.classList.add('card');
   
    const CardContentElement  = document.createElement('div');
    CardContentElement.classList.add('card-content');
    const mediaElement = document.createElement('div');
    mediaElement.classList.add('media');
    const mediacontentElement = document.createElement('div');
    mediacontentElement.classList.add('media-content');
    const titleis4Element = document.createElement('p');
    titleis4Element.innerText = 'EventID: ' +context.currentTarget.EventID;
    titleis4Element.classList.add('title');
    titleis4Element.classList.add('is-4');
    const subtitleis6TicketPriceElement = document.createElement('p');
    subtitleis6TicketPriceElement.innerText = 'Price of the ticket:'+ context.currentTarget.TicketPrice;
    subtitleis6TicketPriceElement.classList.add('subtitle');
    subtitleis6TicketPriceElement.classList.add('is-6');

    const subtitleis6TotalTicketsElement = document.createElement('p');
    subtitleis6TotalTicketsElement.innerText = 'Event ID: '+context.currentTarget.EventID;
    subtitleis6TotalTicketsElement.classList.add('subtitle');
    subtitleis6TotalTicketsElement.classList.add('is-6');
{/* <div class="field">
  <div class="control">
    <input class="input is-primary" type="text" placeholder="Primary input">
  </div>
</div> */}

   const fieldElement = document.createElement('div');
   fieldElement.classList.add('field');
   const controlElement = document.createElement('div');
   controlElement.classList.add('control');
   const TextLabelElement = document.createElement('label');
   TextLabelElement.innerText = 'Email ID: ';
   TextLabelElement.classList.add('subtitle');
   TextLabelElement.classList.add('is-6');
   const EmailElement = document.createElement('input');
   EmailElement.id="EmailID";
   EmailElement.classList.add('email');
   EmailElement.classList.add('input');
   EmailElement.classList.add('is-primary');
   EmailElement.setAttribute('type','email');
   
//    //TicketCOunt
//    <div class="control">
//   <div class="select">
//     <select>
//       <option>Select dropdown</option>
//       <option>With options</option>
//     </select>
//   </div>
// </div>
const controlticketcountElement = document.createElement('div');
controlticketcountElement.classList.add('control');
const selectticketcountElement = document.createElement('div');
selectticketcountElement.classList.add('select');
const selectElement = document.createElement('select');
selectElement.classList.add('NumberOfTickets');
for(var i=1; i<=10; i++)
{
    optionElement = document.createElement('option');
    optionElement.innerText = i;
    selectElement.appendChild(optionElement);
}  
selectticketcountElement.appendChild(selectElement);
controlticketcountElement.appendChild(selectticketcountElement);   
   
   
   controlElement.appendChild(TextLabelElement);
   controlElement.appendChild(EmailElement);
   fieldElement.appendChild(controlElement);
 







//     <div class="button-section">
//<button class="button is-primary is-rounded">Rounded</button>
//     <a class="button is-info GetAllEvents">
//       Search All Events
//   </a>
//   </div>
    const buttonsectionElement  = document.createElement('div');
    buttonsectionElement.classList.add('button-section');
        const buttonisprimaryroundedElement = document.createElement('button');
        buttonisprimaryroundedElement.classList.add('button');
        buttonisprimaryroundedElement.classList.add('is-primary');
        buttonisprimaryroundedElement.classList.add('is-rounded');
        buttonisprimaryroundedElement.classList.add('EventID'+context.currentTarget.EventID);
        buttonisprimaryroundedElement.classList.add('OpenPurchaseModal');
        buttonisprimaryroundedElement.innerText = 'Buy';
        buttonisprimaryroundedElement.addEventListener('click', commitpurchase);
        buttonisprimaryroundedElement.TicketPrice = context.currentTarget.TicketPrice;
        buttonisprimaryroundedElement.EventID = context.currentTarget.EventID;
        

    buttonsectionElement.appendChild(buttonisprimaryroundedElement);

    mediacontentElement.appendChild(titleis4Element);
    mediacontentElement.appendChild(subtitleis6TicketPriceElement);
    mediacontentElement.appendChild(subtitleis6TotalTicketsElement);
    mediacontentElement.appendChild(fieldElement);
    mediacontentElement.appendChild(controlticketcountElement);
    mediacontentElement.appendChild(buttonsectionElement);
    mediaElement.appendChild(mediacontentElement);
    CardContentElement.appendChild(mediaElement);
    cardElement.appendChild(CardContentElement);

    columnElement.appendChild(cardElement);
    ticketpricesElement.appendChild(columnElement);


}


async function commitpurchase(context)
{
    const Email = document.querySelector('.email').value;
    const NumberOfTickets = document.querySelector('.NumberOfTickets').value;
    const EventID = context.currentTarget.EventID;
    const TicketPrice = context.currentTarget.TicketPrice;
    // alert(Email);
    // alert(EventID);
    // alert(TicketPrice);
    // alert(NumberOfTickets);
    var URL = POSTPURCHASE_URL+ "?eventid=" + EventID + "&ticketprice=" +  TicketPrice + "&email=" + Email + "&ticketcount=" + NumberOfTickets
    // alert(URL);
    fetch(URL, {
  method: 'POST'
})
  .then(response => {
    console.log(response)
    if(response.status == 200)
     {
         alert('purchase successful. You will be redirected to home page!');
         ticketpricesElement.innerHTML='';
     }
     if(response.status == 403)
     {
         alert('duplicate purchase found. You cannot re-purchase same ticket slab under a used EmailID!');
         
     }
     if(response.status != 200 && response.status != 403)
     {
         alert('Heavens closed without notice! Try again later!');
         
     }
  })
  .catch(err => {
    console.log(err)
  })




}


// async function OpenPurchaseModal1(context)
// {
//   alert(context.currentTarget.TicketPrice);
//   <div class="modal">
//   <div class="modal-background"></div>
//   <div class="modal-card">
//     <header class="modal-card-head">
//       <p class="modal-card-title">Modal title</p>
//       <button class="delete" aria-label="close"></button>
//     </header>
//     <section class="modal-card-body">
//       <!-- Content ... -->
//     </section>
//     <footer class="modal-card-foot">
//       <button class="button is-success">Save changes</button>
//       <button class="button">Cancel</button>
//     </footer>
//   </div>
// </div>
  
// const modalElement = document.createElement('div');
// modalElement.classList.add('modal');
// const modalbackgroundElement = document.createElement('div');
// modalbackgroundElement.classList.add('modal-background');
// modalElement.appendChild(modalbackgroundElement);
// const modalCardElement = document.createElement('div');
// modalElement.classList.add('modal-card');

// const modalCardHeadElement = document.createElement('header');
// modalCardHeadElement.classList.add('modal-card-head');
// const modalcardtitleElement = document.createElement('p');
// modalcardtitleElement.classList.add('modal-card-title');
// modalcardtitleElement.innerText='Purchase Review';
// const deleteElement = document.createElement('button');
// deleteElement.classList.add('delete');
// deleteElement.setAttribute('aria-label','close');
// modalCardHeadElement.appendChild(modalcardtitleElement);
// modalCardHeadElement.appendChild(deleteElement);

// const ModalcardBody = document.createElement('section');
// ModalcardBody.classList.add('modal-card-body');
// const modalbodyTextElement = document.createElement('p');
// modalbodyTextElement.innerText=context.currentTarget.TicketPrice;
// ModalcardBody.appendChild(modalbodyTextElement);

// const ModalCardFooter = document.createElement('footer');
// ModalCardFooter.classList.add('modal-card-foot');
// const purchaseButtonElement = document.createElement('button');
// purchaseButtonElement.classList.add('button');
// purchaseButtonElement.classList.add('is-success');
// const cancelButtonElement = document.createElement('button');
// cancelButtonElement.classList.add('button');
// ModalCardFooter.appendChild(purchaseButtonElement);
// ModalCardFooter.appendChild(cancelButtonElement);

// modalCardElement.appendChild(modalCardHeadElement);
// modalCardElement.appendChild(ModalcardBody);
// modalCardElement.appendChild(ModalCardFooter);
// modalElement.appendChild(modalCardElement);

// EventListElement.appendChild(modalElement);








//}

GetAllEventsElement.addEventListener('click', GetAllEvents);
//GetAllEvents();