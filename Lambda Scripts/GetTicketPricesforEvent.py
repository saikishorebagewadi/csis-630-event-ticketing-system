import pymysql
import json

#Configuration
endpoint='eventcoremysql.cvhjqlot6ius.us-east-1.rds.amazonaws.com'
username='admin'
password='...'
database_name='EventsDB2020'

#Connection
connection = pymysql.connect(endpoint,user=username,passwd=password,db=database_name)

def GetTicketPricesforEvent(event,context):
    cursor = connection.cursor()
    print(json.dumps(event['queryStringParameters']))
    queryparms = event['queryStringParameters']
    cursor.execute('CALL EventsDB2020.uspGetTicketPricesforEvent('+ str(queryparms['eventid']) +')')
    print(json.dumps(event))
    rows = cursor.fetchall()
    row_headers=[x[0] for x in cursor.description] #this will extract row headers
    json_data=[]
    for result in rows:
        json_data.append(dict(zip(row_headers,result)))
    return {
        'statusCode': 200,
        'headers': {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        },

        'body': json.dumps(json_data),
        #'custom': json.dumps(event)
    }





#abc = GetTicketPricesforEvent({'queryStringParameters':1},2)
#print(abc)
