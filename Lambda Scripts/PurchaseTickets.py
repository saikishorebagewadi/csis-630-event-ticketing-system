import pymysql
import json

#Configuration
endpoint='eventcoremysql.cvhjqlot6ius.us-east-1.rds.amazonaws.com'
username='admin'
password='...'
database_name='EventsDB2020'

#Connection
connection = pymysql.connect(endpoint,user=username,passwd=password,db=database_name)

def PurchaseTickets(event,context):
    cursor = connection.cursor()
#   print(json.dumps(event['queryStringParameters']))
    queryparms = event['queryStringParameters']
    query = 'CALL EventsDB2020.uspPurchaseTicket('+ str(queryparms['eventid'])+','+str(queryparms['ticketprice'])+',\''+str(queryparms['email'])+'\','+str(queryparms['ticketcount'])+')'
    json_data=''
    message=''
    returncode=200
    try:
        message = cursor.execute(query)
        connection.commit()
        json_data=[{"status": "Purchase success"}]
    except:
        returncode=403
        json_data=[{"status": "Purchase failed", "reason": "(customlog) internal error"}]  
    
    print(json.dumps(query))
    print(json.dumps(message))
#    row_headers=[x[0] for x in cursor.description] #this will extract row headers
    
#    for result in rows:
#        json_data.append(dict(zip(row_headers,result)))

    return {
        'statusCode': returncode,
        'headers': {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        },

        'body': json.dumps(json_data),
#        'custom': json.dumps(event)
    }




#abc = PurchaseTickets({'queryStringParameters':{'eventid':1,'ticketprice':440,'email':'sai4@gmail.com','ticketcount':1}},2)
#print(abc)
