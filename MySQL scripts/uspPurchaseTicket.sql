CREATE PROCEDURE uspPurchaseTicket ( IN evntID int, IN tktPrice int,IN email varchar(50),IN ticketcount int)
BEGIN
 INSERT INTO EVENT_SALES VALUES(evntID,email,tktPrice,ticketcount);
 UPDATE INVENTORY
 SET TicketsSold = TicketsSold + ticketcount, TicketsAvailable = TicketsAvailable - ticketcount
 WHERE EventID=evntID AND TicketPrice = tktPrice;
END
