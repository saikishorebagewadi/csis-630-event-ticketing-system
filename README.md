# CSIS 630 Event Ticketing System
# Youtube Link: https://www.youtube.com/watch?v=gdS2wzgMYCg 

## Deployment instructions:
 ### Part I:
    1. Spin up a new AMAONZ RDS DB. allow Public access in permissions
    2. Connect to it via MySQL Workbench from your local desktop using the Server name and credentials.
    3. Use Scripts provided in the database section here and deploy the tables and Stored procedures

 ### Part II:
    1. Create three Lambda functions using the three Lambda Python methods provided in the Lambda Folder.
    2. Create three API Gateway endpoints on Amazon API and point them to the three Lambdas you created in step above.
    3. Edit OPTIONS method of API gateway endpoints to enable CORS headers in response.

 ### Part III:
    1. Create a new Amaozon S3 bucket and copy the index.html as well as app.js to it. while copying, enable public access on these files in Advacnced Options section.
    2. Now in configurastion section of the S3 bucket, select option to enable Static Website Hosting.
    3. At this point the Web Paghe should be available and fully functional.


## Debug suggestion:
     Ensure  the SPs all work before you trigeer them from Lambda, then look at running Lambda function locally on Visual studio code by uncommenting last two lines of code in lambda functions(i marked and left the calling statements commented). If those work, next, look at API endpoints if they all produce results. Majority of the issues should be resolved in this area of implementation. If you get standard CORS error message, check if you have enabled Response headers “Access-Control-Allow-Origin”,” Access-Control-Allow-Methods”,” Access-Control-Allow-Headers” to be sent back to browser.

